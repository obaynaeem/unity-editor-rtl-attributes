
# About this project

The project aim to help developers in dealing with RTL strings in the Unity inspector in a more efficient way using C# atributes and Unity editor scripting.



## Prerequisites
The attributes are built on top of the awesome
 [TMP RTL plugin](https://github.com/pnarimani/RTLTMPro).
Please download the plugin so that the attibutes work as intended.
## installation
Go to the releases page and download the unity package, import it into your project and you're ready. 
## How to use
For for any string vaiable, you can either use the "RTLText" attribute or the "RTLTextArea".
```c#
    [SerializeField, RTLText] private string normalField;
    // just like the normal TextArea attribute
    // you can specify the min and max size of the text area 
    // with the RTLTextArea attribute
    [SerializeField, RTLTextArea(2,4)] private string areaField;
```
In the inspector, a foldout icon will appear next the string field, Expand it to read the string in the correct format.
## Contribution
All contributions are welcome to further improve this project.

Contact:
obaynaeem@gmail.com