using UnityEngine;
using UnityEditor;
using System;

[CustomPropertyDrawer(typeof(RTLTextAreaAttribute))]
public class RTLTextAreaProperty : RTLTextPropertyBase<RTLTextAreaAttribute>
{
    #region Fields

    #region Rects
    private Rect orginalTextScrollRect;
    private Rect rtlTextScrollRect;
    #endregion

    #region Floats
    private float spacing = 5f;
    private float areaLineHeight = 15f;
    private float labelHeight = 15f;
    private float textAreaHeight;
    private float scrollRectHeight;
    private float maxAreaHeight;
    private float minAreaHeight;
    #endregion

    #region Vectors
    private Vector2 scrollPos;
    #endregion

    #endregion

    #region Setup Methods
    /// <summary>
    /// Setup property fileds and variables
    /// </summary>
    /// <param name="label"></param>
    protected override void SetupProperty(RTLTextAreaAttribute customAttr)
    {
        maxAreaHeight = customAttr.maxLines * areaLineHeight;
        minAreaHeight = customAttr.minLines * areaLineHeight;
    }

    #endregion

    #region Unity Methods
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        // if the property is not init then initlize it
        if (!Init) InitValues(label);
        //begain drawing the property
        EditorGUI.BeginProperty(position, label, property);
        // validate the property type
        if (!ValidateField(property.propertyType.ToString()))
        {
            EditorGUI.HelpBox(position, "RTl Text Area Attribute should only be used with strings", MessageType.Error);
            EditorGUI.EndProperty();
            return;
        }
        // get the property value
        string value = property.stringValue;
        // get the fixed arabic word in the value
        string fixedValue = GetFixedText(value);
        // calculate the whole property height on the inspector
        CalculateAreaHeight(value);
        // calculate the property rects positions
        CalculateRectPositions(position);
        // begin the check if there is a user input
        EditorGUI.BeginChangeCheck();
        // draw rects and get the current input value
        string newValue = DrawRects(property, label, value, fixedValue);
        //property.serializedObject.ApplyModifiedProperties();
        //property.serializedObject.Update();
        // if there is an input then assign the new value to the propery 
        if (EditorGUI.EndChangeCheck())
        {
            property.stringValue = newValue;
            //foreach (var item in ActiveEditorTracker.sharedTracker.activeEditors)
            //    if (item.serializedObject == BaseObject)
            //    { item.Repaint(); return; }
            //HandleUtility.Repaint();
            //EditorUtility.SetDirty(property.serializedObject.targetObject);
        }
        // end drawing the property
        EditorGUI.EndProperty();
    }
    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        // if the property is not init then initlize it
        if (!Init) InitValues(label);

        float propertyHeight = CalculatePropertyHeight();
        return property.isExpanded ? propertyHeight : (propertyHeight / 2f);
    }

    #endregion

    #region Calculations Methods
    /// <summary>
    /// calculate the text area input height
    /// </summary>
    /// <param name="areaContent"></param>
    private void CalculateAreaHeight(string areaContent)
    {
        // wrap the text area content into GUi Content for height calculations
        GUIContent content = new GUIContent(areaContent);
        // get the content height
        float currentTextAreaHeight = textAreaStyle.CalcHeight(content, EditorGUIUtility.currentViewWidth);
        // if the content height is smaller then min allowed height
        // then assign the min height to the scroll hieght and the text area
        if (currentTextAreaHeight < minAreaHeight)
        {
            scrollRectHeight = minAreaHeight;
            textAreaHeight = minAreaHeight;
            verticalScrollbarStyle = GUIStyle.none;
        }
        // if the content height is larger then max allowed height
        // then assign the max height to the scroll hieght
        else if (currentTextAreaHeight > maxAreaHeight)
        {
            scrollRectHeight = maxAreaHeight;
            textAreaHeight = currentTextAreaHeight;
            verticalScrollbarStyle = GUI.skin.verticalScrollbar;
        }
        // otherwise use the content height for both the scroll and text area
        else
        {
            scrollRectHeight = currentTextAreaHeight;
            textAreaHeight = currentTextAreaHeight;
            verticalScrollbarStyle = GUIStyle.none;
        }
    }
    /// <summary>
    /// calculate the rect position for this property
    /// </summary>
    /// <param name="stratPosition"></param>
    protected override void CalculateRectPositions(Rect stratPosition)
    {
        Vector3 currentPosition = stratPosition.position;
        Vector3 currentSize = stratPosition.size;
        currentPosition.y += spacing / 2f;

        orginalTextLabelRect = new Rect(currentPosition, new Vector2(currentSize.x, labelHeight));

        currentSize = orginalTextLabelRect.size;
        currentPosition.y += currentSize.y + spacing;

        orginalTextRect = new Rect(currentPosition, new Vector2(currentSize.x, textAreaHeight));

        //if (!renderRTLSection) return;

        currentSize = orginalTextRect.size;
        currentPosition.y += scrollRectHeight + spacing;


        rtlTextLabelRect = new Rect(currentPosition, new Vector2(currentSize.x, labelHeight));
        currentSize = rtlTextLabelRect.size;
        currentPosition.y += currentSize.y + spacing;

        rtlTextRect = new Rect(currentPosition, new Vector2(currentSize.x, textAreaHeight));
        currentPosition.y += currentSize.y + spacing;


        orginalTextScrollRect = new Rect(orginalTextRect.position, new Vector2(orginalTextRect.size.x, scrollRectHeight));
        rtlTextScrollRect = new Rect(rtlTextRect.position, new Vector2(rtlTextRect.size.x, scrollRectHeight));
    }

    #endregion

    #region RTl Text Methods

    protected override string GetFixedText(string input)
    {
        //string value = base.GetFixedText(input);
        string value = string.Empty;
        string[] lines = input.Split("\n");
        foreach(string line in lines)
        {
            value += base.GetFixedText(line);
            value += "\n";
        }
        //finalText.Reverse();
        //value = finalText.ToString();
        //return value.ReverseLines(true);
        return value;
    }

    #endregion

    #region Draw Methods
    /// <summary>
    /// draw this property rects on the inspector
    /// </summary>
    /// <param name="label"></param>
    /// <param name="orginalValue"></param>
    /// <param name="fixedValue"></param>
    /// <returns></returns>
    protected override string DrawRects(SerializedProperty property,GUIContent label,string orginalValue,string fixedValue)
    {
        // draw the first text area label
        property.isExpanded = EditorGUI.Foldout(orginalTextLabelRect, property.isExpanded, label);
        // draw a rect and inside it the texts area
        scrollPos = GUI.BeginScrollView(orginalTextScrollRect, scrollPos, orginalTextRect, true, true, GUIStyle.none, verticalScrollbarStyle);
        string newValue = EditorGUI.TextArea(orginalTextRect, orginalValue, textAreaStyle);
        GUI.EndScrollView();

        if (!property.isExpanded) return newValue; 
        // disable this scope
        // it's a readonly scope
        // draw the rtl text area label
        EditorGUI.LabelField(rtlTextLabelRect, rtlLabel);
        // draw a rect and inside it the texts area
        GUI.BeginScrollView(rtlTextScrollRect, scrollPos, rtlTextRect, true, true, GUIStyle.none, verticalScrollbarStyle);
        EditorGUI.BeginDisabledGroup(true);
        EditorGUI.TextArea(rtlTextRect, fixedValue, textAreaStyle);
        EditorGUI.EndDisabledGroup();
        //EditorGUI.TextArea(rtlTextRect, fixedValue, textAreaStyle);
        GUI.EndScrollView();


        return newValue;
    }

    #endregion

    /// <summary>
    ///  validate the field type 
    /// </summary>
    /// <param name="fieldType"></param>
    /// <returns></returns>
    private bool ValidateField(string fieldType)
    {
        // check the type of the field
        if (fieldType != nameof(String))
        {
            return false;
        }

        return true;
    }

    public override float CalculatePropertyHeight()
    {
        return (scrollRectHeight) * 2 +
        // two labels
        (labelHeight * 2) + (spacing * 4);
    }
}
