using UnityEngine;
using UnityEditor;
using System;

[CustomPropertyDrawer(typeof(RTLTextAttribute))]
public class RTLTextProperty : RTLTextPropertyBase<RTLTextAttribute>
{

    #region Floats
    private float spacing = 5f;
    private float labelHeight = 15f;
    #endregion

    #region Unity Methods
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        // if the property is not init then initlize it
        if (!Init) InitValues(label);
        //begain drawing the property
        EditorGUI.BeginProperty(position, label, property);
        // validate the property type
        if (!ValidateField(property.propertyType.ToString()))
        {
            EditorGUI.HelpBox(position, "RTl Text Area Attribute should only be used with strings", MessageType.Error);
            EditorGUI.EndProperty();
            return;
        }
        // get the property value
        string value = property.stringValue;
        // get the fixed arabic word in the value
        string fixedValue = GetFixedText(value);
        // calculate the property rects positions
        CalculateRectPositions(position);
        // begin the check if there is a user input
        EditorGUI.BeginChangeCheck();
        // draw rects and get the current input value
        string newValue = DrawRects(property, label, value, fixedValue);
        // if there is an input then assign the new value to the propery 
        if (EditorGUI.EndChangeCheck())
            property.stringValue = newValue;
        // end drawing the property
        EditorGUI.EndProperty();
    }

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        // if the property is not init then initlize it
        if (!Init) InitValues(label);
        float propertyHeight = CalculatePropertyHeight();
        return property.isExpanded ? propertyHeight : (propertyHeight / 2f);
    }
    #endregion
    #region Draw Methods
    /// <summary>
    /// draw this property rects on the inspector
    /// </summary>
    /// <param name="label"></param>
    /// <param name="orginalValue"></param>
    /// <param name="fixedValue"></param>
    /// <returns></returns>
    protected override string DrawRects(SerializedProperty property, GUIContent label, string orginalValue, string fixedValue)
    {
        // draw the first text area label
        property.isExpanded = EditorGUI.Foldout(orginalTextLabelRect, property.isExpanded, label);
        // draw a rect and inside it the texts area
        string newValue = EditorGUI.TextField(orginalTextRect, orginalValue, textAreaStyle);


        if (!property.isExpanded) return newValue;
        // disable this scope
        // it's a readonly scope
        // draw the rtl text area label
        EditorGUI.LabelField(rtlTextLabelRect, rtlLabel);

        EditorGUI.BeginDisabledGroup(true);
        // draw a rect and inside it the texts area
        EditorGUI.TextField(rtlTextRect, fixedValue, textAreaStyle);
        
        EditorGUI.EndDisabledGroup();

        return newValue;
    }

    #endregion

    #region Calculations Methods
    /// <summary>
    /// calculate the rect position for this property
    /// </summary>
    /// <param name="stratPosition"></param>
    protected override void CalculateRectPositions(Rect stratPosition)
    {
        //backgroundRect = new Rect(stratPosition.position, new Vector2(stratPosition.size.x, GetPropertyHeight()));
        Vector3 currentPosition = stratPosition.position;
        Vector3 currentSize = stratPosition.size;
        currentPosition.y += spacing / 2f;

        orginalTextLabelRect = new Rect(currentPosition, new Vector2(currentSize.x, labelHeight));

        currentSize = orginalTextLabelRect.size;
        currentPosition.y += currentSize.y + spacing;

        orginalTextRect = new Rect(currentPosition, new Vector2(currentSize.x, EditorGUIUtility.singleLineHeight));
        
        //if (renderRTLSection) return;

        currentSize = orginalTextRect.size;
        currentPosition.y += EditorGUIUtility.singleLineHeight + spacing;

        rtlTextLabelRect = new Rect(currentPosition, new Vector2(currentSize.x, labelHeight));
        currentSize = rtlTextLabelRect.size;
        currentPosition.y += currentSize.y + spacing;

        rtlTextRect = new Rect(currentPosition, new Vector2(currentSize.x, EditorGUIUtility.singleLineHeight));
        currentPosition.y += currentSize.y + spacing;
    }

    #endregion
    private bool ValidateField(string fieldType)
    {
        // check the type of the field
        if (fieldType != nameof(String))
        {
            return false;
        }

        return true;
    }

    public override float CalculatePropertyHeight()
    {
        // two labels + two string fields
        return ((labelHeight * 2) + EditorGUIUtility.singleLineHeight * 2)
            // 4 spaces inbetween
            + spacing * 4f;
    }
}
