using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using RTLTMPro;
using System;

public abstract class RTLTextPropertyBase<T> : PropertyDrawer where T : RTLTextAttributeBase
{
    #region Attribute Refrence
    protected T customAttr;
    #endregion

    #region Styles
    protected GUIStyle textAreaStyle;
    protected GUIStyle verticalScrollbarStyle;
    #endregion

    #region String Builder
    protected FastStringBuilder finalText;
    #endregion

    #region Rects
    protected Rect orginalTextLabelRect;
    protected Rect orginalTextRect;
    protected Rect rtlTextLabelRect;
    protected Rect rtlTextRect;
    #endregion

    #region Booleans
    protected bool Init { get; private set; }
    #endregion

    #region GUI Content
    protected GUIContent rtlLabel;
    #endregion

    protected void InitValues(GUIContent label)
    {
        // get the attribute rewfrence
        customAttr = (T)fieldInfo.GetCustomAttributes(typeof(T), true)[0];
        // init the text area style
        textAreaStyle = new GUIStyle(EditorStyles.textArea);
        // assign the rtl label name
        rtlLabel = new GUIContent(label.text + " (RTL)");
        // init the string builder
        finalText = new FastStringBuilder(RTLSupport.DefaultBufferSize);
        // mark proerty as initlized
        Init = true;

        SetupProperty(customAttr);
    }
    protected virtual void SetupProperty(T attribute) { }
    protected abstract void CalculateRectPositions(Rect stratPosition);
    public override abstract float GetPropertyHeight(SerializedProperty property, GUIContent label);
    public abstract float CalculatePropertyHeight();
    public override abstract void OnGUI(Rect position, SerializedProperty property, GUIContent label);
    protected abstract string DrawRects(SerializedProperty property, GUIContent label, string orginalValue, string fixedValue);
    /// <summary>
    /// Get a string correct format for arabic
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    protected virtual string GetFixedText(string input)
    {
        // if the input in smpty return the saem input
        if (string.IsNullOrEmpty(input))
            return input;

        finalText.Clear();
        // fix the letters order
        RTLSupport.FixRTL(input, finalText, false, true, true);
        // reverse the order for correct displaying
        string reverseValue = finalText.ToString();

        return reverseValue;
    }
    private bool ValidateField(string fieldType)
    {
        // check the type of the field
        if (fieldType != nameof(String))
        {
            return false;
        }

        return true;
    }

}
