using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RtlTestScript : MonoBehaviour
{
    [SerializeField, RTLText] private string normalField;
    // just like the normal TextArea attribute
    // you can specify the min and max size of the text area 
    // with the RTLTextArea attribute
    [SerializeField, RTLTextArea(2, 4)] private string areaField;
}



