using System;

[AttributeUsage(AttributeTargets.Field, AllowMultiple = false)]
public class RTLTextAttribute : RTLTextAttributeBase
{
    public RTLTextAttribute() { }
}
