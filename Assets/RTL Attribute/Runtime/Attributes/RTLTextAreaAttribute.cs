using UnityEngine;
using System;

[AttributeUsage(AttributeTargets.Field, AllowMultiple = false)]
public class RTLTextAreaAttribute : RTLTextAttributeBase
{
    // minumum lines to draw the text area in
    public readonly int minLines;
    // maximum lines to draw the text area in
    public readonly int maxLines;
    public RTLTextAreaAttribute()
    {
        minLines = 3;
        maxLines = 3;
    }
    public RTLTextAreaAttribute(int minLines, int maxLines)
    {
        // validate the input
        if(!ValidateInput(minLines, maxLines))
        {
            // if validation failed
            // assign the defalut values
            this.minLines = 3;
            this.minLines = 3;
            return;
        }
        // else assign the passed values
        this.minLines = minLines;
        this.maxLines = maxLines;
    }
    private bool ValidateInput(int minLines, int maxLines)
    {
        // check if some parameter is smaller then zero
        if (minLines <= 0 || maxLines <= 0)
        {
            Debug.LogWarning("[RTL Text Area Attribute] lines values should be postive integers");
            return false;
        }
        // check if the max lines is smaller the min lines
        if (maxLines < minLines)
        {
            Debug.LogWarning("[RTL Text Area Attribute] max lines value should be equal or greater to min lines value");
            return false;
        }

        return true;
    }
}
