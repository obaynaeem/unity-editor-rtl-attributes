using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static  class HelperExtentions
{
    public static string ReverseLines(this string value,bool reverseWord)
    {
        string rtlValue = string.Empty;
        // spli the value into lines for correcting the lines order
        string[] lines = value.Split("\n");
        foreach (string line in lines)
        {
            rtlValue += reverseWord ? ReverseWord(line) : line;
            rtlValue += "\n";
        }
        return rtlValue;
    }
    public static string ReverseWord(string inout)
    {
        string result = string.Empty;
        for (int i = 0; i < inout.Length; i++)
        {
            result += inout[inout.Length - i - 1];
        }
        return result;
    }
}
